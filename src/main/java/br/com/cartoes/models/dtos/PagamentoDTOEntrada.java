package br.com.cartoes.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PagamentoDTOEntrada {
    @NotNull
    private int cartao_id;

    @NotNull(message = "A descrição da compra não pode ser null.")
    @NotBlank(message = "A descrição da compra não pode estar em branco.")
    @Size(min = 2, max = 400, message = "A descrição da compra deve ter no mínimo 2 caracteres. ")
    private String descricao;

    @NotNull
    private double valor;

    public PagamentoDTOEntrada() {
    }

    public int getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(int cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
