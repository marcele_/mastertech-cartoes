package br.com.cartoes.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "A descrição da compra não pode ser null.")
    @NotBlank(message = "A descrição da compra não pode estar em branco.")
    @Size(min = 2, max = 400, message = "A descrição da compra deve ter no mínimo 2 caracteres. ")
    private String descricao;

    @NotNull
    private double valor;

    @ManyToOne
    private Cartao cartao;

    public Pagamento() {
    }

    public Pagamento(@NotNull(message = "A descrição da compra não pode ser null.") @NotBlank(message = "A descrição da compra não pode estar em branco.") @Size(min = 2, max = 400, message = "A descrição da compra deve ter no mínimo 2 caracteres. ") String descricao, @NotNull double valor, Cartao cartao) {
        this.descricao = descricao;
        this.valor = valor;
        this.cartao = cartao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}
