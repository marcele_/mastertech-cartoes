package br.com.cartoes.controllers;

import br.com.cartoes.models.dtos.PagamentoDTOEntrada;
import br.com.cartoes.models.dtos.PagamentoDTOSaida;
import br.com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @GetMapping("/pagamentos/{id_cartao}")
    public Iterable<PagamentoDTOSaida> buscarPorIdCartao(@Valid @PathVariable(name = "id_cartao") int idCartao){
        try{
            return pagamentoService.buscarPorIdCartao(idCartao);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PagamentoDTOSaida> cadastrar(@RequestBody @Valid PagamentoDTOEntrada pagamentoDTOEntrada){
        try{
            return ResponseEntity.status(201).body(pagamentoService.salvar(pagamentoDTOEntrada));
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
