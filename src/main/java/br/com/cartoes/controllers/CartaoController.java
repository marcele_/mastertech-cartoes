package br.com.cartoes.controllers;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.dtos.CartaoDTOEntrada;
import br.com.cartoes.models.dtos.CartaoDTOSaida;
import br.com.cartoes.models.dtos.CartaoDTOSaidaAtivoOculto;
import br.com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/cartoes")
    public Iterable<Cartao> listar(){
        try {
            return cartaoService.listarTodos();
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @GetMapping("/cartao/{numero}")
    public CartaoDTOSaidaAtivoOculto buscarPorId(@Valid @PathVariable(name = "numero") String numero){
        try{
            return cartaoService.buscarPorId(numero);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/cartao")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CartaoDTOSaida> cadastrar(@RequestBody @Valid CartaoDTOEntrada cartaoDTOEntrada){
        try{
            return ResponseEntity.status(201).body(cartaoService.salvar(cartaoDTOEntrada));
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PatchMapping("/cartao/{numero}")
    public CartaoDTOSaida alteraStatusAtivo(@Valid @PathVariable(name = "numero" ) String numero, @RequestBody CartaoDTOSaida cartaoDTOSaida){
        try{
            return cartaoService.alteraStatusAtivo(numero, cartaoDTOSaida);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }
}
