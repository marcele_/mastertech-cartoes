package br.com.cartoes.controllers;

import br.com.cartoes.models.Cliente;
import br.com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping ("/clientes")
    public Iterable<Cliente> listar(){
        try{
            return clienteService.listarTodos();
        } catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/cliente/{id}")
    public Optional<Cliente> buscarPorId(@Valid @PathVariable(name = "id") int id){
        try{
            return clienteService.buscarPorId(id);
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());
        }
    }

    @PostMapping("/cliente")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Cliente> cadastrar(@RequestBody @Valid Cliente cliente){
        try{
            return ResponseEntity.status(201).body(clienteService.salvar(cliente));
        }catch (Exception ex){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.getMessage());

        }
    }
}
