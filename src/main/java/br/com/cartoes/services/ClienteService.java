package br.com.cartoes.services;

import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Iterable<Cliente> listarTodos(){
        return clienteRepository.findAll();
    }

    public Optional<Cliente> buscarPorId(int id){
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (clienteOptional.isPresent()){
            return clienteOptional;
        } else {
            throw new RuntimeException("O cliente "+ id + " não foi encontrado.");
        }
    }

    public Cliente salvar(Cliente cliente){
        return clienteRepository.save(cliente);
    }

}
