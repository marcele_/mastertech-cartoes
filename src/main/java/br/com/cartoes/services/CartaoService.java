package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.models.dtos.CartaoDTOEntrada;
import br.com.cartoes.models.dtos.CartaoDTOSaida;
import br.com.cartoes.models.dtos.CartaoDTOSaidaAtivoOculto;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public Iterable<Cartao> listarTodos(){
        return cartaoRepository.findAll();
    }

    public CartaoDTOSaidaAtivoOculto buscarPorId(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent()){
            CartaoDTOSaidaAtivoOculto cartaoDTOSaidaAtivoOculto = new CartaoDTOSaidaAtivoOculto(
                    cartaoOptional.get().getId(), cartaoOptional.get().getNumero(), cartaoOptional.get().getCliente().getId());
            return cartaoDTOSaidaAtivoOculto;
        } else {
            throw new RuntimeException("O cartão nº "+ numero +" não foi encontrado!");
        }
    }

    public CartaoDTOSaida salvar(CartaoDTOEntrada cartaoDTOEntrada){
        Optional<Cliente> clienteOptional = clienteRepository.findById(cartaoDTOEntrada.getClienteId());
        if (clienteOptional.isPresent()){
            Cliente cliente = new Cliente();
            Cartao cartao = new Cartao(cartaoDTOEntrada.getNumero(), false, clienteOptional.get());
            Cartao cartaoSalvo = cartaoRepository.save(cartao);
            CartaoDTOSaida cartaoDTOSaida = new CartaoDTOSaida(
                    cartaoSalvo.getId(), cartaoSalvo.getNumero(), cartaoSalvo.getCliente().getId(), cartaoSalvo.isAtivo());
            return cartaoDTOSaida;
        }
        else
        {
            throw new RuntimeException("O cliente "+ cartaoDTOEntrada.getClienteId()+" não foi encontrado!");
        }
    }

    public CartaoDTOSaida alteraStatusAtivo(String numero, CartaoDTOSaida cartaoDTOSaida){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);
        if (cartaoOptional.isPresent()){
            cartaoOptional.get().setAtivo(cartaoDTOSaida.isAtivo());

            Cartao cartao = new Cartao(cartaoOptional.get().getNumero(),cartaoOptional.get().isAtivo(), cartaoOptional.get().getCliente());
            cartao.setId(cartaoOptional.get().getId());

            Cartao cartaoSalvo = cartaoRepository.save(cartao);

            cartaoDTOSaida = new CartaoDTOSaida(
                    cartaoSalvo.getId(), cartaoSalvo.getNumero(), cartaoSalvo.getCliente().getId(), cartaoSalvo.isAtivo());
            return cartaoDTOSaida;
        } else {
            throw new RuntimeException("O cartão não foi encontrado!");
        }
    }

}
