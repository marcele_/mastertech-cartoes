package br.com.cartoes.services;

import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.models.dtos.PagamentoDTOEntrada;
import br.com.cartoes.models.dtos.PagamentoDTOSaida;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public Iterable<PagamentoDTOSaida> buscarPorIdCartao(int idCartao){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(idCartao);

        if (cartaoOptional.isPresent()){
            Cartao cartao = new Cartao();
            cartao = cartaoOptional.get();

            List<Pagamento> listaLancamentos = pagamentoRepository.findAllByCartao(cartao);
            List<PagamentoDTOSaida> listaPagamentoDTOSaida = new ArrayList<>();

            for (Pagamento pagamentoObjeto: listaLancamentos) {
                PagamentoDTOSaida pagamentoDTOSaida = new PagamentoDTOSaida(
                        pagamentoObjeto.getId(), pagamentoObjeto.getCartao().getId(),
                        pagamentoObjeto.getDescricao(), pagamentoObjeto.getValor());

                listaPagamentoDTOSaida.add(pagamentoDTOSaida);
            }

            return listaPagamentoDTOSaida;
        } else {
            throw new RuntimeException("O cartão não foi encontrado!");
        }
    }

    public PagamentoDTOSaida salvar(PagamentoDTOEntrada pagamentoDTOEntrada){

        Optional<Cartao> cartaoOptional = cartaoRepository.findById(pagamentoDTOEntrada.getCartao_id());
        if (cartaoOptional.isPresent()){

            Pagamento pagamento = new Pagamento(pagamentoDTOEntrada.getDescricao(), pagamentoDTOEntrada.getValor(),
                    cartaoOptional.get());
            Pagamento pagamentoRealizado = pagamentoRepository.save(pagamento);

            PagamentoDTOSaida pagamentoDTOSaida = new PagamentoDTOSaida(
                    pagamentoRealizado.getId(), pagamentoRealizado.getCartao().getId(),
                    pagamentoRealizado.getDescricao(), pagamentoRealizado.getValor());
            return pagamentoDTOSaida;
        }
        else
        {
            throw new RuntimeException("O cartão não foi encontrado e o pagamento não foi realizado!");
        }
    }

}
